/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2018 - 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include <math.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <float.h>
#include <getopt.h>
#include <errno.h>
#include <time.h>

#include "edflib.h"
#include "utils.h"

#define PROGRAM_NAME       "edfgenerator"
#define PROGRAM_VERSION    "1.21"

#define FILETYPE_EDF       (0)
#define FILETYPE_BDF       (1)

#define WAVE_SINE          (0)
#define WAVE_SQUARE        (1)
#define WAVE_RAMP          (2)
#define WAVE_TRIANGLE      (3)
#define WAVE_WHITE_NOISE   (4)
#define WAVE_PINK_NOISE    (5)

#define EDF_MAX_CHNS    (4096)

#define EDF_MAX_ANNOTS    (64)



struct sig_par_struct
{
  int sf[EDF_MAX_CHNS];
  int digmax[EDF_MAX_CHNS];
  int digmin[EDF_MAX_CHNS];
  int waveform[EDF_MAX_CHNS];

  double w[EDF_MAX_CHNS];
  double q[EDF_MAX_CHNS];
  double sine_1[EDF_MAX_CHNS];
  double square_1[EDF_MAX_CHNS];
  double triangle_1[EDF_MAX_CHNS];
  double signalfreq[EDF_MAX_CHNS];
  double physmax[EDF_MAX_CHNS];
  double physmin[EDF_MAX_CHNS];
  double peakamp[EDF_MAX_CHNS];
  double dutycycle[EDF_MAX_CHNS];
  double dc_offset[EDF_MAX_CHNS];
  double phase[EDF_MAX_CHNS];

  double b0[EDF_MAX_CHNS];
  double b1[EDF_MAX_CHNS];
  double b2[EDF_MAX_CHNS];
  double b3[EDF_MAX_CHNS];
  double b4[EDF_MAX_CHNS];
  double b5[EDF_MAX_CHNS];
  double b6[EDF_MAX_CHNS];

  char physdim[EDF_MAX_CHNS][32];

  double *buf[EDF_MAX_CHNS];

  int *randbuf[EDF_MAX_CHNS];
} sig_par;


edflib_annotation_t annot[EDF_MAX_ANNOTS];

int num_annots=0;


void print_help(void);
int parse_annot_arg(const char *);
long long atofu(const char *);


int main(int argc, char **argv)
{
  int i, j, k, n, chan, sum,
      err,
      option_index=0,
      c=0,
      fd=-1,
      hdl=-1,
      filetype=0,
      duration=30,
      digmax_set=0,
      digmin_set=0,
      datrecs=0,
      datrecs_set=0,
      datrecduration_set=0,
      merge_set=0,
      chns=1,
      edf_chns=1,
      datrec_delay_ms=0,
      annot_chan_pos=EDF_ANNOT_IDX_POS_END,
      starttime_hour=0,
      starttime_minute=0,
      starttime_second=0,
      subsecond_starttime=0;

  double datrecduration=1,
         ftmp,
         white_noise,
         *merge_buf=NULL;

  char str[1024]="",
       *s_ptr=NULL;

  const char waveforms_str[6][16]=
  {
    "sine",
    "square",
    "ramp",
    "triangle",
    "white-noise",
    "pink-noise"
  };

  struct timespec tp;

  setlocale(LC_ALL, "C");

  setlinebuf(stdout);
  setlinebuf(stderr);

  memset(&sig_par, 0, sizeof(struct sig_par_struct));

  for(i=0; i<EDF_MAX_CHNS; i++)
  {
    sig_par.sf[i] = 500;
    sig_par.w[i] = 1;
    sig_par.q[i] = 1;
    sig_par.signalfreq[i] = 10;
    sig_par.physmax[i] = 1200;
    sig_par.physmin[i] = -1200;
    sig_par.peakamp[i] = 1000;
    sig_par.dutycycle[i] = 50;
    sig_par.phase[i] = 0;
    strlcpy(sig_par.physdim[i], "uV", 32);
  }

  struct option long_options[] = {
    {"type",            required_argument, 0, 0},  /*  0 */
    {"len",             required_argument, 0, 0},  /*  1 */
    {"rate",            required_argument, 0, 0},  /*  2 */
    {"freq",            required_argument, 0, 0},  /*  3 */
    {"wave",            required_argument, 0, 0},  /*  4 */
    {"dcycle",          required_argument, 0, 0},  /*  5 */
    {"phase",           required_argument, 0, 0},  /*  6 */
    {"physmax",         required_argument, 0, 0},  /*  7 */
    {"physmin",         required_argument, 0, 0},  /*  8 */
    {"amp",             required_argument, 0, 0},  /*  9 */
    {"unit",            required_argument, 0, 0},  /* 10 */
    {"digmax",          required_argument, 0, 0},  /* 11 */
    {"digmin",          required_argument, 0, 0},  /* 12 */
    {"offset",          required_argument, 0, 0},  /* 13 */
    {"datrecs",         required_argument, 0, 0},  /* 14 */
    {"datrec-duration", required_argument, 0, 0},  /* 15 */
    {"signals",         required_argument, 0, 0},  /* 16 */
    {"merge",           no_argument,       0, 0},  /* 17 */
    {"delay",           required_argument, 0, 0},  /* 18 */
    {"starttime",       required_argument, 0, 0},  /* 19 */
    {"annot-chan-pos",  required_argument, 0, 0},  /* 20 */
    {"annotation",      required_argument, 0, 0},  /* 21 */
    {"help",            no_argument,       0, 0},  /* 22 */
    {0, 0, 0, 0}
  };

  if(argc == 1)  print_help();

  while(1)
  {
    c = getopt_long_only(argc, argv, "", long_options, &option_index);

    if(c == -1)  break;

      if(option_index == 16)  /* signals */
      {
        chns = atoi(optarg);
        if((chns < 1) || (chns >= EDF_MAX_CHNS))
        {
          fprintf(stderr, "illegal value for option %s, must be in the range 1 to %i\n", long_options[option_index].name, EDF_MAX_CHNS - 1);
          return EXIT_FAILURE;
        }
        if(chns == 1)
        {
          merge_set = 0;
        }
      }
  }

  optind = 1;

  while(1)
  {
    c = getopt_long_only(argc, argv, "", long_options, &option_index);

    if(c == -1)  break;

    if(c == 0)
    {
      if(option_index < 17)
      {
        if(optarg == NULL)
        {
          fprintf(stderr, "missing value for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }
      }

      if(option_index == 0)  /* type */
      {
        if(!strcmp(optarg, "edf"))
        {
          filetype = FILETYPE_EDF;
        }
        else if(!strcmp(optarg, "bdf"))
          {
            filetype = FILETYPE_BDF;
          }
          else
          {
            fprintf(stderr, "unrecognized value for option %s\n", long_options[option_index].name);
            return EXIT_FAILURE;
          }
      }

      if(option_index == 1)  /* len */
      {
        duration = atoi(optarg);
        if(duration < 1)
        {
          fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }
      }

      if((option_index >= 2) && (option_index <= 13))  /* signal parameters */
      {
        for(n=0; n<chns; n++)
        {
          if(!n)
          {
            s_ptr = strtok(optarg, ",");
          }
          else
          {
            s_ptr = strtok(NULL, ",");
          }
          if(s_ptr == NULL)
          {
            for(k=n; k<chns; k++)
            {
              if(option_index == 2)  /* rate */
              {
                sig_par.sf[k] = sig_par.sf[k-1];
              }
              if(option_index == 3)  /* freq */
              {
                sig_par.signalfreq[k] = sig_par.signalfreq[k-1];
              }
              if(option_index == 4)  /* wave */
              {
                sig_par.waveform[k] = sig_par.waveform[k-1];
              }
              if(option_index == 5)  /* dcycle */
              {
                sig_par.dutycycle[k] = sig_par.dutycycle[k-1];
              }
              if(option_index == 6)  /* phase */
              {
                sig_par.phase[k] = sig_par.phase[k-1];
              }
              if(option_index == 7)  /* physmax */
              {
                sig_par.physmax[k] = sig_par.physmax[k-1];
              }
              if(option_index == 8)  /* physmin */
              {
                sig_par.physmin[k] = sig_par.physmin[k-1];
              }
              if(option_index == 9)  /* amp */
              {
                sig_par.peakamp[k] = sig_par.peakamp[k-1];
              }
              if(option_index == 10)  /* unit */
              {
                strlcpy(sig_par.physdim[k], sig_par.physdim[n-1], 16);
              }
              if(option_index == 11)  /* digmax */
              {
                sig_par.digmax[k] = sig_par.digmax[k-1];
              }
              if(option_index == 12)  /* digmin */
              {
                sig_par.digmin[k] = sig_par.digmin[k-1];
              }
              if(option_index == 13)  /* offset */
              {
                sig_par.dc_offset[k] = sig_par.dc_offset[k-1];
              }
            }

            break;
          }

          if(option_index == 2)  /* rate */
          {
            sig_par.sf[n] = atoi(s_ptr);
            if(sig_par.sf[n] < 1)
            {
              fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
              return EXIT_FAILURE;
            }
          }

          if(option_index == 3)  /* freq */
          {
            sig_par.signalfreq[n] = atof(s_ptr);
            if((sig_par.signalfreq[n] < 9.99999e-3) || (sig_par.signalfreq[n] > (sig_par.sf[n] / 4.0 + 1e-6)))
            {
              fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
              return EXIT_FAILURE;
            }
          }

          if(option_index == 4)  /* wave */
          {
            for(i=0; i<6; i++)
            {
              if(!strcmp(s_ptr, waveforms_str[i]))
              {
                sig_par.waveform[n] = i;
                break;
              }
            }
            if(i==6)
            {
              fprintf(stderr, "unrecognized value for option %s\n", long_options[option_index].name);
              return EXIT_FAILURE;
            }
          }

          if(option_index == 5)  /* dcycle */
          {
            sig_par.dutycycle[n] = atof(s_ptr);
            if((sig_par.dutycycle[n] < 0.099999) || (sig_par.dutycycle[n] > 100.000001))
            {
              fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
              return EXIT_FAILURE;
            }
          }

          if(option_index == 6)  /* phase */
          {
            sig_par.phase[n] = atof(s_ptr);
            if((sig_par.phase[n] <= -360) || (sig_par.phase[n] >= 360))
            {
              fprintf(stderr, "illegal value for option %s, must be in the range -359.999999 to 359.999999\n", long_options[option_index].name);
              return EXIT_FAILURE;
            }
            sig_par.phase[n] = fmod(sig_par.phase[n] + 360, 360);
          }

          if(option_index == 7)  /* physmax */
          {
            sig_par.physmax[n] = atof(s_ptr);
          }

          if(option_index == 8)  /* physmin */
          {
            sig_par.physmin[n] = atof(s_ptr);
          }

          if(option_index == 9)  /* amp */
          {
            sig_par.peakamp[n] = atof(s_ptr);
          }

          if(option_index == 10)  /* unit */
          {
            strlcpy(sig_par.physdim[n], s_ptr, 16);
          }

          if(option_index == 11)  /* digmax */
          {
            sig_par.digmax[n] = atoi(s_ptr);
            digmax_set = 1;
          }

          if(option_index == 12)  /* digmin */
          {
            sig_par.digmin[n] = atoi(s_ptr);
            digmin_set = 1;
          }

          if(option_index == 13)  /* offset */
          {
            sig_par.dc_offset[n] = atof(s_ptr);
          }
        }

//         if(n != chns)
//         {
//           fprintf(stderr, "found %i parameters for option %s but expected %i\n", n, long_options[option_index].name, chns);
//           return EXIT_FAILURE;
//         }
      }

      if(option_index == 14)  /* datrecs */
      {
        datrecs = atoi(optarg);
        if(datrecs < 1)
        {
          fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }
        datrecs_set = 1;
      }

      if(option_index == 15)  /* datrec-duration */
      {
        datrecduration = atof(optarg);
        if((datrecduration < 0.001) || (datrecduration > 60))
        {
          fprintf(stderr, "illegal value for option %s, must be in the range 0.001 to 60\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }
        datrecduration_set = 1;
      }

      if(option_index == 17)  /* merge */
      {
        if(chns > 1)
        {
          merge_set = 1;
        }
      }

      if(option_index == 18)  /* delay */
      {
        datrec_delay_ms = atoi(optarg);
        if((datrec_delay_ms < 1) || (datrec_delay_ms > 10000))
        {
          fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }
      }

      if(option_index == 19)  /* subsecond starttime */
      {
        if(((strlen(optarg) != 12) && ((strlen(optarg) != 15))) ||
           (optarg[2] != ':') || (optarg[5] != ':') || (optarg[8] != '.') ||
           !isdigit(optarg[0]) || !isdigit(optarg[1]) || !isdigit(optarg[3]) ||
           !isdigit(optarg[4]) || !isdigit(optarg[6]) || !isdigit(optarg[7]) ||
           !isdigit(optarg[9]) || !isdigit(optarg[10]) || !isdigit(optarg[11]))
        {
          fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        starttime_hour = atoi(optarg);
        starttime_minute = atoi(optarg + 3);
        starttime_second = atoi(optarg + 6);
        if((starttime_hour > 23) || (starttime_minute > 59) || (starttime_second > 59))
        {
          fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        if(strlen(optarg) == 15)
        {
          for(i=0; i<6; i++)
          {
            if(!isdigit(optarg[9+i]))
            {
              fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
              return EXIT_FAILURE;
            }
          }

          subsecond_starttime = atoi(optarg + 9);
        }
        else
        {
          for(i=0; i<3; i++)
          {
            if(!isdigit(optarg[9+i]))
            {
              fprintf(stderr, "illegal value for option %s\n", long_options[option_index].name);
              return EXIT_FAILURE;
            }
          }

          subsecond_starttime = atoi(optarg + 9) * 1000;
        }
      }

      if(option_index == 20)  /* annotation channel position */
      {
        if(!strcmp(optarg, "before"))
        {
          annot_chan_pos = EDF_ANNOT_IDX_POS_START;
        }
        else if(!strcmp(optarg, "middle"))
          {
            annot_chan_pos = EDF_ANNOT_IDX_POS_MIDDLE;
          }
          else if(!strcmp(optarg, "after"))
            {
              annot_chan_pos = EDF_ANNOT_IDX_POS_END;
            }
            else
            {
              fprintf(stderr, "unrecognized value for option %s\n", long_options[option_index].name);
              return EXIT_FAILURE;
            }
      }

      if(option_index == 21)  /* annotation */
      {
        for(n=0; ; n++)
        {
          if(!n)
          {
            s_ptr = strtok(optarg, ",");
          }
          else
          {
            s_ptr = strtok(NULL, ",");
          }

          if(s_ptr == NULL)  break;

          if(parse_annot_arg(s_ptr))
          {
            fprintf(stderr, "unrecognized value for option %s\n", long_options[option_index].name);
            return EXIT_FAILURE;
          }
        }
      }

      if(option_index == 22)
      {
        print_help();
        return EXIT_SUCCESS;
      }
    }
  }

  if(optind < argc)
  {
    fprintf(stderr, "unrecognized argument(s):");
    while(optind < argc)
    {
      fprintf(stderr, " %s", argv[optind++]);
    }
    fprintf(stderr, "\n--help for help\n");
    return EXIT_FAILURE;
  }

  for(chan=0; chan<chns; chan++)
  {
    if(!digmax_set)
    {
      if(filetype == FILETYPE_EDF)
      {
        sig_par.digmax[chan] = 32767;
      }
      else
      {
        sig_par.digmax[chan] = 8388607;
      }
    }

    if(!digmin_set)
    {
      if(filetype == FILETYPE_EDF)
      {
        sig_par.digmin[chan] = -32768;
      }
      else
      {
        sig_par.digmin[chan] = -8388608;
      }
    }

    if(fabs(sig_par.physmax[chan]) > 9999999.5)
    {
      fprintf(stderr, "error signal %i: physical maximum must be <= 9999999 and >= -9999999\n", chan + 1);
      return EXIT_FAILURE;
    }

    if(fabs(sig_par.physmin[chan]) > 9999999.5)
    {
      fprintf(stderr, "error signal %i: physical maximum must be <= 9999999 and >= -9999999\n", chan + 1);
      return EXIT_FAILURE;
    }

    if(sig_par.peakamp[chan] <= 0.0009999)
    {
      fprintf(stderr, "error signal %i: peak amplitude must be >= 0.001\n(amp: %f)\n", chan + 1, sig_par.peakamp[chan]);
      return EXIT_FAILURE;
    }

    if(sig_par.physmax[chan] > sig_par.physmin[chan])
    {
      if(sig_par.physmax[chan] < ((sig_par.peakamp[chan] * 0.999999) + sig_par.dc_offset[chan]))
      {
        fprintf(stderr, "error signal %i: physical maximum must be higher than peak amplitude + DC-offset\n"
                        "peak amplitude: %f   physical maximum: %f\n", chan + 1, sig_par.peakamp[chan], sig_par.physmax[chan]);
        return EXIT_FAILURE;
      }

      if(sig_par.physmin[chan] > ((sig_par.peakamp[chan] * -0.999999) + sig_par.dc_offset[chan]))
      {
        fprintf(stderr, "error signal %i: physical minimum must be lower than peak amplitude + DC-offset\n"
                        "peak amplitude: %f   physical minimum: %f\n", chan + 1, sig_par.peakamp[chan], sig_par.physmin[chan]);
        return EXIT_FAILURE;
      }
    }
    else
    {
      if(sig_par.physmax[chan] > ((sig_par.peakamp[chan] * 1.000001) + sig_par.dc_offset[chan]))
      {
        fprintf(stderr, "error signal %i: physical maximum must be higher than peak amplitude + DC-offset\n"
                        "peak amplitude: %f   physical maximum: %f\n", chan + 1, sig_par.peakamp[chan], sig_par.physmax[chan]);
        return EXIT_FAILURE;
      }

      if(sig_par.physmin[chan] < ((sig_par.peakamp[chan] * -1.000001) + sig_par.dc_offset[chan]))
      {
        fprintf(stderr, "error signal %i: physical minimum must be lower than peak amplitude + DC-offset\n"
                        "peak amplitude: %f   physical minimum: %f\n", chan + 1, sig_par.peakamp[chan], sig_par.physmin[chan]);
        return EXIT_FAILURE;
      }
    }

    if(filetype == FILETYPE_BDF)
    {
      if(sig_par.digmax[chan] > 8388607)
      {
        fprintf(stderr, "error signal %i: digital maximum must be <= 8388607\n", chan + 1);
        return EXIT_FAILURE;
      }

      if(sig_par.digmin[chan] < -8388608)
      {
        fprintf(stderr, "error signal %i: digital minimum must be >= -8388608\n", chan + 1);
        return EXIT_FAILURE;
      }
    }
    else if(filetype == FILETYPE_EDF)
      {
        if(sig_par.digmax[chan] > 32767)
        {
          fprintf(stderr, "error signal %i: digital maximum must be <= 32767\n", chan + 1);
          return EXIT_FAILURE;
        }

        if(sig_par.digmin[chan] < -32768)
        {
          fprintf(stderr, "error signal %i: digital minimum must be >= -32768\n", chan + 1);
          return EXIT_FAILURE;
        }
      }

      if(sig_par.digmin[chan] >= sig_par.digmax[chan])
      {
        fprintf(stderr, "error signal %i: digital minimum must be less than digital maximum\n", chan + 1);
        return EXIT_FAILURE;
      }

    if(merge_set)
    {
      if(chan)
      {
        if(sig_par.sf[chan] != sig_par.sf[0])
        {
          fprintf(stderr, "error signal %i: option --merge requires that all signals have equal samplerate\n", chan + 1);
          return EXIT_FAILURE;
        }

        if(sig_par.physmax[chan] != sig_par.physmax[0])
        {
          fprintf(stderr, "error signal %i: option --merge requires that all signals have equal value for physical maximum\n", chan + 1);
          return EXIT_FAILURE;
        }

        if(sig_par.physmin[chan] != sig_par.physmin[0])
        {
          fprintf(stderr, "error signal %i: option --merge requires that all signals have equal value for physical minimum\n", chan + 1);
          return EXIT_FAILURE;
        }

        if(sig_par.digmax[chan] != sig_par.digmax[0])
        {
          fprintf(stderr, "error signal %i: option --merge requires that all signals have equal value for digital maximum\n", chan + 1);
          return EXIT_FAILURE;
        }

        if(sig_par.digmin[chan] != sig_par.digmin[0])
        {
          fprintf(stderr, "error signal %i: option --merge requires that all signals have equal value for digital minimum\n", chan + 1);
          return EXIT_FAILURE;
        }

        if(strcmp(sig_par.physdim[chan], sig_par.physdim[0]))
        {
          fprintf(stderr, "error signal %i: option --merge requires that all signals have equal physical dimension (units)\n", chan + 1);
          return EXIT_FAILURE;
        }
      }
    }
  }

  if(datrecduration_set)
  {
    duration = (duration / datrecduration) + 0.5;
    if(duration < 1)
    {
      duration = 1;
    }

    if(!datrecs_set)
    {
      datrecs = duration;
    }
  }
  else
  {
    datrecs = duration;
  }

  for(i=0; i<EDF_MAX_CHNS; i++)
  {
    sig_par.buf[i] = NULL;
  }

  for(i=0; i<chns; i++)
  {
    sig_par.buf[i] = (double *)calloc(1, sizeof(double[sig_par.sf[i]]));
    if(sig_par.buf[i]==NULL)
    {
      fprintf(stderr, "Malloc error line %i file %s\n", __LINE__, __FILE__);
      return EXIT_FAILURE;
    }

    if((sig_par.waveform[i] == WAVE_WHITE_NOISE) || (sig_par.waveform[i] == WAVE_PINK_NOISE))  /* white or pink noise */
    {
      sig_par.randbuf[i] = (int *)calloc(1, sizeof(int[sig_par.sf[i]]));
      if(sig_par.randbuf[i]==NULL)
      {
        fprintf(stderr, "Malloc error line %i file %s\n", __LINE__, __FILE__);
        return EXIT_FAILURE;
      }
    }
  }

  if(chns == 1)
  {
    snprintf(str, 1024, "edfgenerator_%fHz_%s_%fHz",
             sig_par.sf[0] / datrecduration, waveforms_str[sig_par.waveform[0]], sig_par.signalfreq[0] / datrecduration);

    if((sig_par.waveform[0] >= WAVE_SQUARE) && (sig_par.waveform[0]<= WAVE_TRIANGLE))
    {
      snprintf(str + strlen(str), 1024 - strlen(str), "_%fpct", sig_par.dutycycle[0]);
    }

    if(sig_par.waveform[0] <= WAVE_TRIANGLE)
    {
      snprintf(str + strlen(str), 1024 - strlen(str), "_%fdegr", sig_par.phase[0]);
    }

    remove_trailing_zeros(str);
  }
  else
  {
    snprintf(str, 1024, "edfgenerator");
  }

  if(merge_set)
  {
    edf_chns = 1;
  }
  else
  {
    edf_chns = chns;
  }

  for(i=0, sum=0; i<edf_chns; i++)
  {
    sum += sig_par.sf[i];
  }

  if(sum >= 5 * 1024 * 1024)
  {
    fprintf(stderr, "error: sum of samplerates of all sigmals exceeds the maximum datrecord size   line %i file %s\n", __LINE__, __FILE__);

    return EXIT_FAILURE;
  }

  if(filetype == FILETYPE_BDF)
  {
    strlcat(str, ".bdf", 1024);

    hdl = edfopen_file_writeonly(str, EDFLIB_FILETYPE_BDFPLUS, edf_chns);
  }
  else if(filetype == FILETYPE_EDF)
    {
      strlcat(str, ".edf", 1024);

      hdl = edfopen_file_writeonly(str, EDFLIB_FILETYPE_EDFPLUS, edf_chns);
    }

  if(hdl<0)
  {
    fprintf(stderr, "error: edfopen_file_writeonly() line %i file %s\n", __LINE__, __FILE__);

    return EXIT_FAILURE;
  }

  if(edf_set_annot_chan_idx_pos(hdl, annot_chan_pos))
  {
    fprintf(stderr, "error: edf_set_annot_chan_idx_pos() line %i file %s\n", __LINE__, __FILE__);

    return EXIT_FAILURE;
  }

  if(datrecduration_set)
  {
    if(edf_set_datarecord_duration(hdl, (int)((datrecduration * 100000) + 0.5)))
    {
      fprintf(stderr, "error: edf_set_datarecord_duration() line %i file %s\n", __LINE__, __FILE__);

      return EXIT_FAILURE;
    }
  }

  if(edf_set_startdatetime(hdl, 2000, 1, 1, starttime_hour, starttime_minute, starttime_second))
  {
    fprintf(stderr, "error: edf_set_startdatetime() line %i file %s\n", __LINE__, __FILE__);

    return EXIT_FAILURE;
  }

  if(edf_set_subsecond_starttime(hdl, subsecond_starttime * 10))
  {
    fprintf(stderr, "error: edf_set_subsecond_starttime() line %i file %s\n", __LINE__, __FILE__);

    return EXIT_FAILURE;
  }

  for(i=0; i<edf_chns; i++)
  {
    if(edf_set_samplefrequency(hdl, i, sig_par.sf[i]))
    {
      fprintf(stderr, "error: edf_set_samplefrequency() line %i file %s\n", __LINE__, __FILE__);

      return EXIT_FAILURE;
    }

    if(edf_set_digital_maximum(hdl, i, sig_par.digmax[i]))
    {
      fprintf(stderr, "error: edf_set_digital_maximum() line %i file %s\n", __LINE__, __FILE__);

      return EXIT_FAILURE;
    }

    if(edf_set_digital_minimum(hdl, i, sig_par.digmin[i]))
    {
      fprintf(stderr, "error: edf_set_digital_minimum() line %i file %s\n", __LINE__, __FILE__);

      return EXIT_FAILURE;
    }

    if(edf_set_physical_maximum(hdl, i, sig_par.physmax[i]))
    {
      fprintf(stderr, "error: edf_set_physical_maximum() line %i file %s\n", __LINE__, __FILE__);

      return EXIT_FAILURE;
    }


    if(edf_set_physical_minimum(hdl, i, sig_par.physmin[i]))
    {
      fprintf(stderr, "error: edf_set_physical_minimum() line %i file %s\n", __LINE__, __FILE__);

      return EXIT_FAILURE;
    }

    if(edf_set_physical_dimension(hdl, i, sig_par.physdim[i]))
    {
      fprintf(stderr, "error: edf_set_physical_dimension() line %i file %s\n", __LINE__, __FILE__);

      return EXIT_FAILURE;
    }

    if(merge_set)
    {
      strlcpy(str, "composite", 18);
    }
    else
    {
      if(sig_par.waveform[i] == WAVE_SINE)
      {
        snprintf(str, 18, "sine %.2fHz", sig_par.signalfreq[i] / datrecduration);
      }
      else if(sig_par.waveform[i] == WAVE_SQUARE)
        {
          snprintf(str, 18, "square %.2fHz", sig_par.signalfreq[i] / datrecduration);
        }
        else if(sig_par.waveform[i] == WAVE_RAMP)
          {
            snprintf(str, 18, "ramp %.2fHz", sig_par.signalfreq[i] / datrecduration);
          }
          else if(sig_par.waveform[i] == WAVE_TRIANGLE)
            {
              snprintf(str, 18, "triangle %.2fHz", sig_par.signalfreq[i] / datrecduration);
            }
            else if(sig_par.waveform[i] == WAVE_WHITE_NOISE)
              {
                snprintf(str, 18, "white noise");
              }
              else if(sig_par.waveform[i] == WAVE_PINK_NOISE)
                {
                  snprintf(str, 18, "pink noise");
                }

      remove_trailing_zeros(str);
    }

    if(edf_set_label(hdl, i, str))
    {
      fprintf(stderr, "error: edf_set_label() line %i file %s\n", __LINE__, __FILE__);

      return EXIT_FAILURE;
    }
  }

  for(i=0; i<chns; i++)
  {
    sig_par.w[i] = M_PI * 2.0;

    sig_par.q[i] = 1.0 / sig_par.sf[i];

    sig_par.w[i] /= (sig_par.sf[i] / sig_par.signalfreq[i]);

    sig_par.sine_1[i] = ((M_PI * 2.0) * sig_par.phase[i]) / 360;

    sig_par.square_1[i] = ((1.0 / sig_par.signalfreq[i]) * sig_par.phase[i]) / 360;

    sig_par.triangle_1[i] = ((1.0 / sig_par.signalfreq[i]) * sig_par.phase[i]) / 360;
  }

  for(i=0; i<chns; i++)
  {
    if((sig_par.waveform[i] == WAVE_WHITE_NOISE) || (sig_par.waveform[i] == WAVE_PINK_NOISE))
    {
      fd = open("/dev/urandom", O_RDONLY | O_NONBLOCK);
      if(fd < 0)
      {
        fprintf(stderr, "error: cannot open /dev/urandom    line %i file %s\n", __LINE__, __FILE__);

        return EXIT_FAILURE;
      }

      break;
    }
  }

  if(merge_set)
  {
    merge_buf = malloc(sizeof(double[sig_par.sf[0]]));
    if(merge_buf == NULL)
    {
      fprintf(stderr, "Malloc error line %i file %s\n", __LINE__, __FILE__);
      return EXIT_FAILURE;
    }
  }

  for(j=0; j<datrecs; j++)
  {
    if(merge_set)
    {
      memset(merge_buf, 0, sizeof(double[sig_par.sf[0]]));
    }

    for(chan=0; chan<chns; chan++)
    {
      if(sig_par.waveform[chan] == WAVE_SINE)
      {
        for(i=0; i<sig_par.sf[chan]; i++)
        {
          sig_par.buf[chan][i] = (sin(sig_par.sine_1[chan]) * sig_par.peakamp[chan]) + sig_par.dc_offset[chan];

          sig_par.sine_1[chan] += sig_par.w[chan];
        }
      }
      else if(sig_par.waveform[chan] == WAVE_SQUARE)
        {
          for(i=0; i<sig_par.sf[chan]; i++)
          {
            ftmp = fmod(sig_par.square_1[chan], 1.0 / sig_par.signalfreq[chan]);

            if((ftmp * sig_par.signalfreq[chan]) < (sig_par.dutycycle[chan] / 100.0))
            {
              sig_par.buf[chan][i] = sig_par.peakamp[chan];
            }
            else
            {
              sig_par.buf[chan][i] = -sig_par.peakamp[chan];
            }
            sig_par.buf[chan][i] += sig_par.dc_offset[chan];

            sig_par.square_1[chan] += sig_par.q[chan];
          }
        }
        else if(sig_par.waveform[chan] == WAVE_RAMP)
          {
            for(i=0; i<sig_par.sf[chan]; i++)
            {
              ftmp = fmod(sig_par.triangle_1[chan], 1.0 / sig_par.signalfreq[chan]);

              if((ftmp * sig_par.signalfreq[chan]) < (sig_par.dutycycle[chan] / 100.0))
              {
                sig_par.buf[chan][i] = sig_par.peakamp[chan] * (200.0 / sig_par.dutycycle[chan]) * ftmp * sig_par.signalfreq[chan] - sig_par.peakamp[chan];
              }
              else
              {
                sig_par.buf[chan][i] = -sig_par.peakamp[chan];
              }
              sig_par.buf[chan][i] += sig_par.dc_offset[chan];

              sig_par.triangle_1[chan] += sig_par.q[chan];
            }
          }
          else if(sig_par.waveform[chan] == WAVE_TRIANGLE)
            {
              for(i=0; i<sig_par.sf[chan]; i++)
              {
                ftmp = fmod(sig_par.triangle_1[chan], 1.0 / sig_par.signalfreq[chan]);

                if((ftmp * sig_par.signalfreq[chan]) < (sig_par.dutycycle[chan] / 200.0))
                {
                  sig_par.buf[chan][i] = sig_par.peakamp[chan] * (400.0 / sig_par.dutycycle[chan]) * ftmp * sig_par.signalfreq[chan] - sig_par.peakamp[chan];
                  sig_par.buf[chan][i] += sig_par.dc_offset[chan];
                }
                else if((ftmp * sig_par.signalfreq[chan]) < (sig_par.dutycycle[chan] / 100.0))
                  {
                    sig_par.buf[chan][i] = sig_par.peakamp[chan] * (400.0 / sig_par.dutycycle[chan]) * ((sig_par.dutycycle[chan] / 100.0) - (ftmp * sig_par.signalfreq[chan])) - sig_par.peakamp[chan];
                  }
                  else
                  {
                    sig_par.buf[chan][i] = -sig_par.peakamp[chan];
                  }
                  sig_par.buf[chan][i] += sig_par.dc_offset[chan];

                  sig_par.triangle_1[chan] += sig_par.q[chan];
              }
            }
            else if((sig_par.waveform[chan] == WAVE_WHITE_NOISE) || (sig_par.waveform[chan] == WAVE_PINK_NOISE))
              {
                err = read(fd, sig_par.randbuf[chan], sizeof(int[sig_par.sf[chan]]));
                if(err != (sig_par.sf[chan] * 4))
                {
                  perror(NULL);
                  fprintf(stderr, "error: read() returned %i   line %i\n", err, __LINE__);

                  return EXIT_FAILURE;
                }

                if(sig_par.waveform[chan] == WAVE_WHITE_NOISE)
                {
                  for(i=0; i<sig_par.sf[chan]; i++)
                  {
                    sig_par.buf[chan][i] = (sig_par.randbuf[chan][i] % ((int)(sig_par.peakamp[chan] * 100.0))) / 100.0;
                    sig_par.buf[chan][i] += sig_par.dc_offset[chan];
                  }
                }
                else if(sig_par.waveform[chan] == WAVE_PINK_NOISE)
                  {
  /* This is an approximation to a -10dB/decade (-3dB/octave) filter using a weighted sum
   * of first order filters. It is accurate to within +/-0.05dB above 9.2Hz
   * (44100Hz sampling rate). Unity gain is at Nyquist, but can be adjusted
   * by scaling the numbers at the end of each line.
   * http://www.firstpr.com.au/dsp/pink-noise/
   */
                    for(i=0; i<sig_par.sf[chan]; i++)
                    {
                      white_noise = (sig_par.randbuf[chan][i] % ((int)(sig_par.peakamp[chan] * 100.0))) / 600.0;
                      sig_par.b0[chan] = 0.99886 * sig_par.b0[chan] + white_noise * 0.0555179;
                      sig_par.b1[chan] = 0.99332 * sig_par.b1[chan] + white_noise * 0.0750759;
                      sig_par.b2[chan] = 0.96900 * sig_par.b2[chan] + white_noise * 0.1538520;
                      sig_par.b3[chan] = 0.86650 * sig_par.b3[chan] + white_noise * 0.3104856;
                      sig_par.b4[chan] = 0.55000 * sig_par.b4[chan] + white_noise * 0.5329522;
                      sig_par.b5[chan] = -0.7616 * sig_par.b5[chan] - white_noise * 0.0168980;
                      sig_par.buf[chan][i] = sig_par.b0[chan] + sig_par.b1[chan] + sig_par.b2[chan] + sig_par.b3[chan] + sig_par.b4[chan] + sig_par.b5[chan] + sig_par.b6[chan] + white_noise * 0.5362;
                      sig_par.buf[chan][i] += sig_par.dc_offset[chan];
                      sig_par.b6[chan] = white_noise * 0.115926;
                    }
                  }
              }

      if(merge_set)
      {
        for(i=0; i<sig_par.sf[chan]; i++)
        {
          merge_buf[i] += sig_par.buf[chan][i];
        }
      }
      else
      {
        if(edfwrite_physical_samples(hdl, sig_par.buf[chan]))
        {
          fprintf(stderr, "error: edfwrite_physical_samples() line %i file %s\n", __LINE__, __FILE__);
          return EXIT_FAILURE;
        }
      }
    }

    if(merge_set)
    {
      if(edfwrite_physical_samples(hdl, merge_buf))
      {
        fprintf(stderr, "error: edfwrite_physical_samples() line %i file %s\n", __LINE__, __FILE__);
        return EXIT_FAILURE;
      }
    }

    if(datrec_delay_ms)
    {
      tp.tv_sec = datrec_delay_ms / 1000;
      tp.tv_nsec = (datrec_delay_ms % 1000) * 1000;

      nanosleep(&tp, NULL);
    }
  }

  if(fd >= 0)
  {
    close(fd);
  }

  edfwrite_annotation_utf8_hr(hdl, 0, -1, "Recording starts");

  edfwrite_annotation_utf8_hr(hdl, datrecs * datrecduration * 1000000 + 0.5, -1, "Recording ends");

  for(i=0; i<num_annots; i++)
  {
    edfwrite_annotation_utf8_hr(hdl, annot[i].onset, annot[i].duration_l, annot[i].annotation);
  }

  edfclose_file(hdl);

  for(i=0; i<chns; i++)
  {
    free(sig_par.buf[i]);
    free(sig_par.randbuf[i]);
  }
  free(merge_buf);

  return EXIT_SUCCESS;
}


int parse_annot_arg(const char *s)
{
  int i, j, len, field_num;

  char delimiter,
       s_onset[256]={""},
       s_duration[256]={""},
       s_description[256]={""};

  len = strlen(s);
  if(len < 7)  return -1;

  if(s[0] != s[len-1])  return -2;

  delimiter = s[0];

  for(i=1, j=1, field_num=0; i<len; i++)
  {
    if(s[i] == delimiter)
    {
      if(i < j + 1)  return -3;

      if(field_num == 0)
      {
        memcpy(s_onset, s + j, i - j);
        s_onset[i-j] = 0;

        if(is_number(s_onset))  return -4;
      }
      else if(field_num == 1)
        {
          memcpy(s_duration, s + j, i - j);
          s_duration[i-j] = 0;

          if(is_number(s_onset))  return -5;
        }
        else if(field_num == 2)
          {
            memcpy(s_description, s + j, i - j);
            s_description[i-j] = 0;
          }

      j = i + 1;

      field_num++;
    }
  }
  if(field_num != 3)  return -6;

  if(num_annots >= EDF_MAX_ANNOTS)  return -7;

  annot[num_annots].onset = atofu(s_onset);
  if(annot[num_annots].onset < 0)  return -8;

  annot[num_annots].duration_l = atofu(s_duration);
  if(annot[num_annots].duration_l < 1)  annot[num_annots].duration_l = -1;

  strlcpy(annot[num_annots].annotation, s_description, EDFLIB_MAX_ANNOTATION_LEN);

  num_annots++;

  return 0;
}


long long atofu(const char *s)
{
  int i, len, dot_pos=-1, has_dot=0;

  long long val1=0, val2=0;

  char str[16]={""};

  len = strlen(s);

  if(len < 1)  return 0;

  val1 = atoll(s) * 1000 * 1000;

  for(i=0; i<len; i++)
  {
    if(s[i] == '.')
    {
      dot_pos = i;
      has_dot = 1;
      break;
    }
  }

  if(!has_dot)  return val1;

  len = strlen(s + dot_pos + 1);
  if(!len)  return val1;

  if(len > 6)
  {
    len = 6;

    memcpy(str, s, 6);
    str[6] = 0;

    val2 = atoll(str);
  }
  else
  {
    val2 = atoll(s + dot_pos + 1);
  }

  for(i=0; i<6-len; i++)
  {
    val2 *= 10;
  }

  return (val1 + val2);
}


void print_help(void)
{
  fprintf(stdout, "\n EDF generator version " PROGRAM_VERSION
    " Copyright (c) 2020 - 2024 Teunis van Beelen   email: teuniz@protonmail.com\n"
    "\n Usage: " PROGRAM_NAME " [OPTION]...\n"
    "\n options:\n"
    "\n --type=edf|bdf default: edf\n"
    "\n --len=file duration in seconds default: 30\n"
    "\n --rate=samplerate in Hertz default: 500 (integer only)\n"
    "\n --freq=signal frequency in Hertz default: 10 (may be a real number e.g. 333.17)\n"
    "\n --wave=sine | square | ramp | triangle | white-noise | pink-noise default: sine\n"
    "\n --dcycle=dutycycle: 0.1-100%% default: 50\n"
    "\n --phase=phase: 0-360degr. default: 0\n"
    "\n --physmax=physical maximum default: 1200 (may be a real number e.g. 1199.99)\n"
    "\n --physmin=physical minimum default: -1200 (may be a real number e.g. -1199.99)\n"
    "\n --amp=peak amplitude default: 1000 (may be a real number e.g. 999.99)\n"
    "\n --unit=physical dimension default: uV\n"
    "\n --digmax=digital maximum default: 32767 (EDF) or 8388607 (BDF) (integer only)\n"
    "\n --digmin=digital minimum default: -32768 (EDF) or -8388608 (BDF) (integer only)\n"
    "\n --offset=physical dc-offset default: 0\n"
    "\n --datrecs=number of datarecords that will be written into the file, has precedence over --len.\n"
    "\n --datrec-duration=duration of a datarecord in seconds default: 1 (may be a real number e.g. 0.25)\n"
    "                   effective samplerate and signal frequency will be inversely proportional to the datarecord duration\n"
    "\n --signals=number of signals default: 1 in case of multiple signals, signal parameters must be separated by a comma e.g.: --rate=1000,800,133\n"
    "\n --merge  merge all signals into one trace, requires equal samplerate and equal physical max/min and equal digital max/min and equal physical dimension (units) for all signals\n"
    "\n --starttime=hh:mm:ss.mmm or hh:mm:ss.uuuuuu in 24h format e.g.: 03:59:59.500, default is 00:00:00.000\n"
    "\n --annotation=/onset/duration/description/[,/onset/duration/description/[,/onset/duration/description/]]\n"
    "               add annotation(s) e.g.: '--annotation=/17.326/2.7/stimulus 1/'\n"
    "               onset: number of seconds since the start of the recording, e.g.: 12 or 4.777.\n"
    "               duration: expressed in seconds, use -1 if unknown or not applicable.\n"
    "               description: string in UTF-8 or ASCII, only printable & readable characters are allowed.\n"
    "               Note: comma's are not allowed in the description string, use quotes if the description contains spaces,\n"
    "               the slash is used as a delimiter and can be changed on the condition that it's not present in the description string.\n"
    "               Example: '--annotation=#17.326#2.7#stimulus 1#,#22.5#1.8#stimulus 2#'\n"
    "\n --delay=milliseconds  emulate streaming by going to sleep for n milliseconds after writing each datarecord, default is 0 (no delay)\n"
    "\n --annot-chan-pos=before | middle | after   default: after\n"
    "                   whether the annotation channel will be put before, in the middle or after the regular channels\n"
    "\n --help\n\n"
    " Note: decimal separator (if any) must be a dot, do not use a comma as a decimal separator\n\n"
  );
}






















